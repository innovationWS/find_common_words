package test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FindCommonWords {

	// Driver code
	public static void main(String[] args) throws IOException {
		
		//Reading all words from file words.txt
		List<String> words = new ArrayList<>();
		List<String> lines = Files.readAllLines(Paths.get("./src/test/words.txt"),Charset.forName("ISO-8859-1"));
		for(String line : lines) {
			words.addAll(Arrays.asList(line.split(" ")));
		}		
		
		//Print top 10 frequent words
		System.out.println(findWord(words.toArray(new String[words.size()])));
		
	}

	public static Map<String, Integer> findWord(String[] arr) {
		// Create HashMap to store word and it's frequency
		HashMap<String, Integer> statisticsMap = new HashMap<String, Integer>();
		// Iterate through array of words
		for (int i = 0; i < arr.length; i++) {
			// If word already exist in HashMap then increase it's count by 1
			if (statisticsMap.containsKey(arr[i])) {
				statisticsMap.put(arr[i], statisticsMap.get(arr[i]) + 1);
			}
			// Otherwise add word to HashMap
			else {
				statisticsMap.put(arr[i], 1);
			}
		}
		//Sort map according it's value				
		return sortMapByValue(statisticsMap);
	}

	public static Map<String, Integer> sortMapByValue(Map<String, Integer> inputMap) {
		return inputMap.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).limit(10).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
	}
}
